const http = require('http');
const { addUser } = require('./routes/addUser');
const { listPrimeUser } = require('./routes/listPrimeUser');
const { takeSubscription } = require('./routes/takeSubscription');

const requestListener = (req, res) => {
  switch (req.url) {
    case '/':
      res.writeHead(200, 'Success');
      res.end(' this is root ');
      break;
    case '/addUser':
      addUser(req, res);
      break;
    case '/takeSubscription':
      takeSubscription(req, res);
      break;
    case '/listPrimeUser':
      listPrimeUser(req, res);
      break;
    default:
      res.writeHead(404, 'Not Found');
      res.end(' Invalid endPoint ');
      break;
  }
};

const server = http.createServer(requestListener);

server.listen(
  5000,
  console.log(`server is up and listening on http://localhost:5000`)
);

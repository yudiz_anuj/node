const aUser = require('../userData/users.json');
const fs = require('fs');

function addUser(req, res) {
  switch (req.method) {
    case 'POST':
      console.log(`method : ${req.method} path : ${req.url}`);

      let data = '';
      req.on('data', (chunk) => {
        data += chunk.toString();
      });
      req.on('end', () => {
        data = JSON.parse(data.toString());
        console.log(`${data.sUserId}`);
        const user = aUser.find((usr) => usr.sUserId == data.sUserId);
        if (user) {
          res.write('User already exists');
        } else {
          oNewUser = {
            sUserId: data.sUserId,
            sEmail: data.sEmail,
            nMobile: data.nMobile,
            nAge: data.nAge,
            sPassword: data.sPassword,
            bprimeStatus: data.bprimeStatus,
            dSubscribedOn: null,
          };
          aUser.push(oNewUser);
          //write to file
          fs.writeFile(
            `./userData/users.json`,
            JSON.stringify(aUser),
            (err) => {
              if (err) throw err;
              else {
                res.writeHead(200, 'Ok');
                res.write('users registered');
              }
            }
          );
        }
        res.end('');
      });
      break;

    default:
      res.write(`current method : ${req.method}`);
      res.end(`\nallowed method : POST`);
  }
}

module.exports = { addUser };

/**
 // write file
fs.writeFile('./file3.txt', 'data for new file 3', (err) => {
  if (err) throw err;
  else console.log('saved');
});
fs.writeFile('./file4.html', 'data for new file 4', (err) => {
  if (err) throw err;
  else console.log('saved');
});
 */

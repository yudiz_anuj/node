const http = require('http');
const fs = require('fs');
const aUser = require('./userData/users.json');
const { addUser } = require('./routes/addUser');
const { listPrimeUser } = require('./routes/listPrimeUser');
const { takeSubscription } = require('./routes/takeSubscription');

const requestListener = (req, res) => {
  switch (req.url) {
    case '/':
      res.writeHead(200, 'Success');
      res.end(' this is root ');
      break;
    case '/addUser':
      addUser(req, res);
      break;
    case '/takeSubscription':
      takeSubscription(req, res);
      break;
    case '/listPrimeUser':
      listPrimeUser(req, res);
      break;
    default:
      res.writeHead(404, 'Not Found');
      res.end(' Invalid endPoint ');
      break;
  }
};

const server = http.createServer(requestListener);

server.listen(
  6000,
  console.log(`server is up and listening on http://localhost:6000`)
);

setInterval(() => {
  aUser.forEach((usr) => {
    if (usr.bprimeStatus === true) {
      usr.nPoints += 1;
    }
  });
  fs.writeFileSync('./userData/users.json', JSON.stringify(aUser));
}, 1000 * 60);

/**
 [
  {
    "sUserId": "anuj",
    "sEmail": "anuj.j@yudiz.in",
    "nMobile": "9876543210",
    "nAge": "22",
    "sPassword": "12345678",
    "bprimeStatus": "true",
    "nPoints": 0
  },
  {
    "sUserId": "bipin",
    "sEmail": "bipin.c@yudiz.in",
    "nMobile": "8967452310",
    "nAge": "21",
    "sPassword": "87654321",
    "bprimeStatus": "true",
    "nPoints": 0
  },
  {
    "sUserId": "sanjay",
    "sEmail": "sanjay.c@yudiz.in",
    "nMobile": "8967452310",
    "nAge": "22",
    "sPassword": "87654321",
    "bprimeStatus": "false",
    "dSubscribedOn": null
  }
]

 */

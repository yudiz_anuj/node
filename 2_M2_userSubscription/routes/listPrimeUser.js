const fs = require('fs');
const aUser = require('../userData/users.json');

function listPrimeUser(req, res) {
  switch (req.method) {
    case 'GET':
      console.log(`method : ${req.method} path : ${req.url}`);

      //filter prime user and store them in a array
      const aPrimeUser = aUser.filter((usr) => usr.bprimeStatus === true);

      let count = 0;
      res.write(`Total Prime Users : ${aPrimeUser.length}\n\n`);

      aPrimeUser.forEach((user) => {
        res.write(`#${++count}\n`);
        res.write(`-----------------------------\n`);
        res.write(`Name   : ${user.sUserId}\n`);
        res.write(`Email  : ${user.sEmail}\n`);
        res.write(`Mobile : ${user.nMobile}\n`);
        res.write(`Age    : ${user.nAge}\n`);
        res.write(`Points : ${user.nPoints}\n`);
        res.write(`--------------<>-------------\n\n`);
      });
      res.end('\n\nAll prime members are listed above :)');
      break;

    default:
      res.write(`current method : ${req.method}`);
      res.end(`\nallowed method : GET`);
  }
}

module.exports = { listPrimeUser };

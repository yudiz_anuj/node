const fs = require('fs');
const aUser = require('../userData/users.json');

function takeSubscription(req, res) {
  switch (req.method) {
    case 'PUT':
      console.log(`method : ${req.method} path : ${req.url}`);

      let data = '';
      req.on('data', (chunk) => {
        data += chunk.toString();
      });
      req.on('end', () => {
        data = JSON.parse(data.toString());
        //filter prime user and store them in a array
        const user = aUser.find((usr) => usr.sUserId === data.sUserId);
        if (user) {
          aUser.forEach((usr) => {
            if (usr.sUserId === data.sUserId && usr.bprimeStatus === false)
              usr.bprimeStatus = true;
          });
          //update file
          fs.writeFile(
            './userData/users.json',
            JSON.stringify(aUser),
            (err) => {
              if (err) throw err;
              else console.log('file updated ');
            }
          );
          res.write('\n\nYou have taken Subscription :)');
        } else {
          res.writeHead(400);
          res.write('user not registered :( \nplease register first :)');
        }

        res.end();
      });

      break;

    default:
      res.write(`current method : ${req.method}`);
      res.end(`\nallowed method : PUT`);
  }
}

module.exports = { takeSubscription };

"use strict";
const express = require("express");
let router = express.Router();

const todoController = require("../controller/todo");

router
  .get("/", todoController.get)
  .post("/", todoController.post)
  .delete("/", todoController.delete);

module.exports = router;

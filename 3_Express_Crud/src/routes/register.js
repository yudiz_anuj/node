"use strict";
const express = require("express");
let router = express.Router();

const registerController = require("../controller/register");

router.post("/", registerController.post);

module.exports = router;

'use strict';
const express = require('express');
let router = express.Router();

const loginController = require('../controller/login');

router.get('/', loginController.get);

module.exports = router;

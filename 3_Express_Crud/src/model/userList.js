let aUser = [
  {
    sId: "yudiz001",
    sName: "Anuj Jha",
    nMobile: 9876543210,
    sEmail: "anuj.j@yudiz.in",
    sPassword: "1111",
    aToDoList: [
      {
        sId: "t01",
        sName: "IoT Assignment",
        dDueBy: "09/03/2021",
      },
    ],
  },
  {
    sId: "yudiz002",
    sName: "Bipin",
    nMobile: 9988776655,
    sEmail: "bipin.c@yudiz.in",
    sPassword: "2222",
    aToDoList: [
      {
        sId: "t01",
        sName: "BDA Assignment",
        dDueBy: "08/03/2021",
      },
    ],
  },
];

function addUser(usr) {
  aUser.push(usr);
}

function getUsers() {
  return aUser;
}

function updateTodo(aTodo, id) {
  aUser.forEach((usr) => {
    if (usr.sId == id) {
      usr.aToDoList = aTodo;
    }
  });
}
module.exports = { addUser, getUsers, updateTodo };

//new Date().toLocaleDateString("en-IN")

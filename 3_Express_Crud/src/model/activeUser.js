const { getUsers } = require("./userList");

let sCurrId = "",
  currUserData = {},
  setCurrId = (id) => {
    sCurrId = id;
  },
  getCurrId = () => {
    return sCurrId;
  },
  getCurrUserData = (sCurrId) => {
    if (sCurrId != "") {
      getUsers().forEach((usr) => {
        if (sCurrId === usr.sId) {
          currUserData = usr;
        }
      });
    }
    return currUserData;
  };

module.exports = { getCurrId, setCurrId, getCurrUserData };

const { addUser, getUsers } = require("../model/userList");

module.exports = {
  post: (req, res) => {
    const sId = req.body.sId,
      sName = req.body.sName,
      nMobile = req.body.nMobile,
      sEmail = req.body.sEmail,
      sPassword = req.body.sPassword,
      aToDoList = req.body.aToDoList;

    const user = getUsers().find((usr) => usr.sId == sId);

    if (user) {
      console.log(`User already exists`);
      res.status(500).send("User already exists");
    } else {
      addUser({
        sId: sId,
        sName: sName,
        nMobile: nMobile,
        sEmail: sEmail,
        sPassword: sPassword,
        aToDoList: aToDoList,
      });
      res.status(500).send("user registered successfully");
    }
  },
};

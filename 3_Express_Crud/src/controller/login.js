const { getUsers } = require("../model/userList");
let { setCurrId, getCurrUserData } = require("../model/activeUser");

module.exports = {
  get: (req, res) => {
    const sId = req.body.sId;
    const sPassword = req.body.sPassword;

    const user = getUsers().find((usr) => usr.sId == sId);

    if (user) {
      if (user.sPassword == sPassword) {
        setCurrId(sId);
        let oUserData = getCurrUserData(sId);
        const sData = `
          Name  : ${oUserData.sName},
          Email : ${oUserData.sEmail},
          Mobile: ${oUserData.nMobile}
        `;
        res.status(200).send(`logged In\n\n${sData}`);
      } else res.status(500).send("Invalid Password");
    } else {
      res.status(500).send("Invalid  Id");
    }
  },
};

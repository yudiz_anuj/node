let { setCurrId, getCurrUserData } = require("../model/activeUser");

module.exports = {
  get: (req, res) => {
    setCurrId("");
    res.status(200).send("logged out");
  },
};

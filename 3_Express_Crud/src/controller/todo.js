const { getUsers, updateTodo } = require("../model/userList");
const { getCurrId, getCurrUserData } = require("../model/activeUser");

module.exports = {
  get: (req, res) => {
    if (getCurrId() == "") {
      res.status(500).send("please login to access your todo");
    } else {
      const aMyTodo = getCurrUserData(getCurrId()).aToDoList;
      res.status(200).send(aMyTodo);
    }
  },
  post: (req, res) => {
    if (getCurrId() == "") {
      res.status(500).send("please login to access your todo");
    } else {
      const sId = req.body.sId,
        sName = req.body.sName,
        dDueBy = req.body.dDueBy,
        aMyTodo = getCurrUserData(getCurrId()).aToDoList;

      aMyTodo.push({
        sId: sId,
        sName: sName,
        dDueBy: dDueBy,
      });
      updateTodo(aMyTodo, getCurrId());
      res.status(200).send("task added successfully");
    }
  },
  delete: (req, res) => {
    if (getCurrId() == "") {
      res.status(500).send("please login to access your todo");
    } else {
      const aMyTodo = getCurrUserData(getCurrId()).aToDoList;
      const aUpdatedTodo = [];
      const sId = req.body.sId;
      aMyTodo.forEach((task) => {
        if (task.sId != sId) {
          aUpdatedTodo.push(task);
        }
      });
      updateTodo(aUpdatedTodo, getCurrId());
      res.status(200).send("task deleted successfully");
    }
  },
};
